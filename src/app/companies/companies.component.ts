import { Component, OnInit } from '@angular/core';
import { Company } from '../juegos/company';
import { CompanyService } from './company.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  showId:boolean = true;
  companies:Company[];

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.companyService.getCompanies().subscribe(
      response => this.companies = response
    );
  }

  changeId(): void{
    this.showId = !this.showId;
  }


}
