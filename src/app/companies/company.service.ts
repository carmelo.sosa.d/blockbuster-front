import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alertas/alert.service';
import { Company } from '../juegos/company';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';

@Injectable()
export class CompanyService {

  urlServer:string = 'http://localhost:8090/';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient, private alertservicio: AlertService, private router: Router, private loginService: LoginService) { }


  getCompanies(): Observable<Company[]>{
    return this.http.get<Company[]>(this.urlServer + 'companies', {headers: this.loginService.getAuthHeaders()}).pipe(
      catchError(e => {
        console.error(`getCompanies error: "${e.message}"`);
        if (e.status == 401){
          this.router.navigate(['/login']);
        }else{
          this.alertservicio.error(`Error al consultar las compañías: "${e.message}"`, {autoClose: true})
        }

        
        return throwError(e);
      })
    );
  }
}
