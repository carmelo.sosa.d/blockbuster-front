import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs'
import { Juego } from './juego';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alertas/alert.service';

@Injectable()
export class JuegoService {

  urlServer:string = 'http://localhost:8090/';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient, private alertservicio: AlertService) { }


  getJuegos(): Observable<Juego[]>{
    //return of(JUEGOS);
    return this.http.get<Juego[]>(this.urlServer + 'juegos').pipe(
      catchError(e => {
        console.error(`getJuegos error: "${e.message}"`);
        this.alertservicio.error(`Error al consultar los juegos: "${e.message}"`, {autoClose: true, keepAfterRouteChange: false})
        return throwError(e);
      })
    );
  }

  create(juego: Juego): Observable<Juego>{
    return this.http.post<Juego>(this.urlServer + 'juegos', juego, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(`create error: "${e.message}"`);
        if (e.status == 400){
          e.error.errorMessage.replace('[','').replace(']','').split(', ').forEach(errorMessage =>
            this.alertservicio.error(errorMessage)
            );          
        }else{
          this.alertservicio.error(`Error al crear el juego: "${e.message}"`);
        }
        return throwError(e);
      })
    );
  }

  getJuego(id: number): Observable<Juego>{
    return this.http.get<Juego>(`${this.urlServer}juegos/${id}`).pipe(
      catchError(e => {
        console.error(`getjuego error: "${e.message}"`);
        this.alertservicio.error(`Error al obtener el juego: "${e.message}"`);
        return throwError(e);
      })
    );
  }

  updateJuego(juego: Juego): Observable<Juego>{
    return this.http.put<Juego>(`${this.urlServer}juegos/${juego.idJuego}`, juego, {headers: this.httpHeaders}).pipe(
      catchError(e => {
        console.error(`update error: "${e.message}"`);
        this.alertservicio.error(`Error al actualizar el juego: "${e.message}"`);
        return throwError(e);
      })
    );
  }

  deleteJuego(id: number): Observable<any> {
    return this.http.delete(`${this.urlServer}juegos/${id}`).pipe(
      catchError(e => {
        console.error(`delete error: "${e.message}"`);
        this.alertservicio.error(`Error al borrar el juego: "${e.message}"`);
        return throwError(e);
      })
    );
  }
}
