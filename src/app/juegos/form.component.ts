import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../alertas/alert.service';
import { CompanyService } from '../companies/company.service';
import { Company } from './company';
import { Juego } from './juego';
import { JuegoService } from './juego.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  juego: Juego = new Juego();
  title:string = 'Crear Juego';
  categorias: any[] = [{title: 'shooter', value: 'SHOOTER'}, {title: 'MOBA', value: 'MOBA'}, {title: 'RPG', value: 'RPG'}, {title: 'Roguelike', value: 'ROGUELIKE'}, {title: 'Metroidvania', value: 'METROIDVANIA'}];
  companies: Company[];
  soloLectura: boolean = false;

  constructor(private juegoService: JuegoService, private router: Router, private companyService: CompanyService, private activateRoute: ActivatedRoute, private alertservicio: AlertService) { }

  ngOnInit(): void {
    this.loadCompanies();
    this.loadJuego();
  }

  create():void {
    this.juegoService.create(this.juego).subscribe(juego => {
      this.alertservicio.success(`Se ha creado correctamente el juego: "${juego.titulo}" con ID: ${juego.idJuego}`, {autoClose: true});
      this.router.navigate(['/juegos']);      
    });
  }

  loadCompanies(): void{
    this.companyService.getCompanies().subscribe(
      companies => this.companies = companies
    );
  }

  loadJuego(): void {
    this.activateRoute.params.subscribe(params => {
      const id = params.id;
      const eliminar = params['delete'];
      if (id) {
        this.title = 'Editar Juego';
        this.juegoService.getJuego(id).subscribe(
          response => this.juego = response
        );
      }else{
        this.title = 'Crear juego';
      }
      if (eliminar){
        this.title = 'Eliminar Juego';        
        this.soloLectura = true;
      }
    });
  }

  compareCompany(companyToCompare: Company, companySelected: Company){
    if (!companyToCompare || !companySelected){
      return false;
    }
    return companyToCompare.idCompany === companySelected.idCompany;
  }

  update():void {
    this.juegoService.updateJuego(this.juego).subscribe( juego => {
      this.alertservicio.success(`Se ha actualizado correctamente el juego "${juego.titulo}" con ID: ${juego.idJuego}`, {autoClose: true});
      this.router.navigate(['/juegos']);
    });
  }

  eliminar():void {
    this.juegoService.deleteJuego(this.juego.idJuego).subscribe(
      response => this.router.navigate(['/juegos'])
    );
  }
}
