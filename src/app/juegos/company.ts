import { Juego } from './juego';

export class Company {
    idCompany: number;
    cif: string;
    nombre: string;
    juegos: Juego[];
}