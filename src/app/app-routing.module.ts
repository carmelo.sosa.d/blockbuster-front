import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesComponent } from './companies/companies.component';
import { FormComponent as JuegosFormComponent} from './juegos/form.component';
import { JuegosComponent } from './juegos/juegos.component';
import { LoginComponent } from './login/login.component';

const ROUTES: Routes = [
  {path: '', redirectTo: '/juegos', pathMatch: 'full'},
  {path: 'juegos', component: JuegosComponent},
  {path: 'companies', component: CompaniesComponent},
  {path: 'juegos/form', component: JuegosFormComponent},
  {path: 'juegos/form/:id', component: JuegosFormComponent},
  {path: 'juegos/form/:delete/:id', component: JuegosFormComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
